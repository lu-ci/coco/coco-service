# Auto Gallery

This is the Coco Auto Gallery.
It's a simpler version of Monika, without Sigma's enormous core.

## So what does Coco do?

Coco periodically checks selected services for new posts and relays them to
a selected Discord channel via webhooks.

## How do you set Coco up?

At the moment, Coco doesn't compile builds automatically,
so you'll have to do that yourself.

1. Clone the repository.
2. Make a `config.yml` based on the example in `data/`.
3. Run `cargo build --release` to build Coco **OR** just run `cargo run --release` to compile and run it.

## What are my Webhook ID and Token?

Discord gives you Webhook URLs like:
```
https://discord.com/api/webhooks/871315725204221962/NZsaUjdmFjWvPIWEbztNc5ZkTFvuWqy_INWKSPpxVGMTfIsBbIWeTpeqyLf1VKnGY14Z
```
- The ID is `871315725204221962`.
- The Token is `NZsaUjdmFjWvPIWEbztNc5ZkTFvuWqy_INWKSPpxVGMTfIsBbIWeTpeqyLf1VKnGY14Z`.
