use crate::core::database::CocoDatabase;
use crate::core::service::CocoCore;
use crate::models::item::{CocoItem, CocoItemWrapper};
use crate::models::pair::CocoChannelPair;
use crate::models::tag::CocoTag;
use serde_json::Value;
use serenity::model::channel::Embed;
use std::any::Any;

#[async_trait::async_trait]
pub trait CocoService: Any + Send + Sync {
    fn id(&self) -> &'static str;
    fn name(&self) -> &'static str;
    fn color(&self) -> u64;
    fn api_url(&self) -> &'static str;
    fn url_base(&self) -> &'static str;

    async fn is_active(&self, db: &CocoDatabase) -> anyhow::Result<bool> {
        let active = CocoChannelPair::active_service(db, self.id()).await?;
        Ok(active)
    }

    fn post_url(&self, eid: i64) -> String {
        format!("{}/{}", self.url_base(), eid)
    }

    fn icon_url(&self) -> &'static str;

    fn make_embed(&self, item: &CocoItem, desc: Option<String>) -> Value {
        Embed::fake(|embed| {
            embed.color(self.color());
            embed.author(|a| {
                a.name(format!("{}: {}", self.name(), item.id));
                a.icon_url(self.icon_url());
                if !self.post_url(item.id).is_empty() {
                    a.url(&item.url);
                }
                a
            });
            embed.image(if let Some(sample) = &item.sample {
                sample
            } else {
                &item.file
            });
            if let Some(desc) = desc {
                embed.description(desc);
            }
            embed.footer(|f| {
                f.text(format!("MD5: {}", &item.md5));
                f
            });
            embed
        })
    }

    async fn collect(&self, core: &CocoCore) -> anyhow::Result<Vec<CocoItemWrapper>>;

    async fn collect_tags(&self) -> anyhow::Result<Vec<CocoTag>> {
        Ok(Vec::new())
    }
}
