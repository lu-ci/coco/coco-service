use crypto::digest::Digest;
use crypto::md5::Md5;
use log::{debug, error};

use crate::core::database::CocoDatabase;
use crate::core::reddit::controller::RedditController;
use crate::core::service::CocoCore;
use crate::core::utility::CocoUtility;
use crate::models::hash::CocoHash;
use crate::models::item::{CocoItem, CocoItemWrapper};
use crate::models::pair::CocoChannelPair;
use crate::services::common::CocoService;

#[derive(Default)]
pub struct RedditService;

#[async_trait::async_trait]
impl CocoService for RedditService {
    fn id(&self) -> &'static str {
        "reddit"
    }

    fn name(&self) -> &'static str {
        "Reddit"
    }

    fn color(&self) -> u64 {
        0xd2_5a_31
    }

    fn api_url(&self) -> &'static str {
        ""
    }

    fn url_base(&self) -> &'static str {
        ""
    }

    fn icon_url(&self) -> &'static str {
        "https://i.imgur.com/7QZGTvi.png"
    }

    async fn collect(&self, core: &CocoCore) -> anyhow::Result<Vec<CocoItemWrapper>> {
        let mut items = Vec::new();
        for sub in Self::sub_names(&core.db).await? {
            debug!("Pulling posts from r/{}...", &sub);
            match self.pull(core, &sub).await {
                Ok(posts) => {
                    for item in posts {
                        item.save(&core.db).await?;
                        let wrapper = CocoItemWrapper::new(&item, self.make_embed(&item, None));
                        items.push(wrapper);
                    }
                }
                Err(err) => {
                    error!("Failed to pull posts from r/{}: {}!", &sub, err);
                }
            }
            tokio::time::sleep(tokio::time::Duration::from_secs(2)).await;
        }
        Ok(items)
    }
}

impl RedditService {
    pub fn boxed() -> Box<dyn CocoService> {
        Box::new(Self::default())
    }

    pub async fn sub_names(db: &CocoDatabase) -> anyhow::Result<Vec<String>> {
        let mut subs = Vec::new();
        let pairs = CocoChannelPair::fetch_active(db).await?;
        for pair in pairs {
            if let Some(settings) = pair.settings {
                for sub in &settings.sources.subs {
                    let sub_name = sub.to_lowercase();
                    if !subs.contains(&sub_name) {
                        subs.push(sub_name)
                    }
                }
            }
        }
        Ok(subs)
    }

    pub async fn hash(&self, core: &CocoCore, id: i64, url: &str) -> anyhow::Result<String> {
        let md5 = match CocoHash::find(&core.db, id, self.id()).await? {
            Some(hash) => hash.hash,
            None => {
                let mut hasher = Md5::new();
                if !core.cfg.core.debug {
                    let resp = reqwest::get(url).await?;
                    let bytes = resp.bytes().await?;
                    hasher.input(bytes.as_ref());
                } else {
                    hasher.input(id.to_string().as_bytes())
                }
                let md5 = hasher.result_str();
                let hash = CocoHash::new(id, self.id(), &md5);
                hash.save(&core.db).await?;
                md5
            }
        };
        Ok(md5)
    }

    pub async fn pull(&self, core: &CocoCore, sub: &str) -> anyhow::Result<Vec<CocoItem>> {
        let mut items = Vec::new();
        let mut controller = RedditController::new(sub);
        controller.get(core).await?;
        for post in controller.posts() {
            if post.is_image() {
                if post.is_gallery {
                    if let Some(gd) = &post.gallery_data {
                        for mi in &gd.items {
                            let item = CocoItem {
                                id: mi.id,
                                md5: self.hash(core, mi.id, &mi.url()).await?,
                                url: format!(
                                    "https://www.reddit.com/r/{}/comments/{}",
                                    sub, post.id
                                ),
                                file: mi.url(),
                                sample: None,
                                tags: vec![],
                                score: post.score,
                                source: self.id().to_string(),
                                source_name: Some(sub.to_string()),
                                active: true,
                                rating: if post.over_18 { 2 } else { 0 },
                                timestamp: post.created_utc as i64,
                            };
                            items.push(item);
                        }
                    }
                } else {
                    let id = (CocoUtility::hash(&post.id) as i64).abs();
                    let item = CocoItem {
                        id,
                        md5: self.hash(core, id, &post.url).await?,
                        url: format!("https://www.reddit.com/r/{}/comments/{}", sub, post.id),
                        file: post.url.to_string(),
                        sample: None,
                        tags: vec![],
                        score: post.score,
                        source: self.id().to_string(),
                        source_name: Some(sub.to_string()),
                        active: true,
                        rating: if post.over_18 { 2 } else { 0 },
                        timestamp: post.created_utc as i64,
                    };
                    items.push(item);
                }
            }
        }
        Ok(items)
    }
}
