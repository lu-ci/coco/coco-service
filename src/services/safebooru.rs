use crate::core::http::CocoHttpClient;
use crate::core::service::CocoCore;
use crate::models::item::{CocoItem, CocoItemWrapper};
use crate::models::tag::CocoTag;
use crate::services::common::CocoService;
use log::debug;
use serde::Deserialize;

#[derive(Default)]
pub struct SafebooruService;

#[async_trait::async_trait]
impl CocoService for SafebooruService {
    fn id(&self) -> &'static str {
        "safebooru"
    }

    fn name(&self) -> &'static str {
        "Safebooru"
    }

    fn color(&self) -> u64 {
        0xf9_f9_f9
    }

    fn api_url(&self) -> &'static str {
        "https://safebooru.org/index.php?page=dapi&s=post&q=index&pid=1&limit=1000&json=1"
    }

    fn url_base(&self) -> &'static str {
        "https://safebooru.org/index.php?page=post&s=view"
    }

    fn post_url(&self, eid: i64) -> String {
        format!("{}&id={}", self.url_base(), eid)
    }

    fn icon_url(&self) -> &'static str {
        "https://i.imgur.com/EghS0El.png"
    }

    async fn collect(&self, core: &CocoCore) -> anyhow::Result<Vec<CocoItemWrapper>> {
        let mut items = Vec::new();
        let native_items = self.pull().await?;
        debug!(
            "Pulled {} native items from {}.",
            native_items.len(),
            self.name()
        );
        for ni in native_items {
            let item = ni.to_item(self);
            item.save(&core.db).await?;
            let wrapper = CocoItemWrapper::new(
                &item,
                self.make_embed(
                    &item,
                    // Some(item.describe(&core.db, self.id(), &item.tags).await?),
                    None,
                ),
            );
            items.push(wrapper);
        }
        Ok(items)
    }

    async fn collect_tags(&self) -> anyhow::Result<Vec<CocoTag>> {
        let mut tags = Vec::new();
        let uri = "https://safebooru.org/index.php?page=dapi&s=tag&q=index&limit=0";
        let resp = CocoHttpClient::get(uri).await?;
        let body = resp.text().await?;
        let ntres = serde_xml_rs::from_str::<SafebooruTagResponse>(&body)?;
        for nt in ntres.tag {
            tags.push(nt.to_tag(self.id()));
        }
        Ok(tags)
    }
}

impl SafebooruService {
    pub fn boxed() -> Box<dyn CocoService> {
        Box::new(Self::default())
    }

    pub async fn pull(&self) -> anyhow::Result<Vec<SafebooruItem>> {
        let resp = CocoHttpClient::get(self.api_url()).await?;
        let body = resp.text().await?;
        Ok(serde_json::from_str(&body)?)
    }
}

#[derive(Deserialize)]
pub struct SafebooruItem {
    pub id: u64,
    directory: String,
    tags: String,
    pub hash: String,
    image: String,
    pub rating: String,
    score: Option<u64>,
    change: u64,
}

impl SafebooruItem {
    pub fn tags(&self) -> Vec<String> {
        let mut tags = Vec::new();
        for tag in self.tags.split(' ').collect::<Vec<&str>>() {
            tags.push(tag.to_string());
        }
        tags
    }

    pub fn file_url(&self) -> String {
        format!(
            "https://safebooru.org/images/{}/{}",
            self.directory, self.image
        )
    }

    pub fn to_item(&self, svc: &SafebooruService) -> CocoItem {
        CocoItem {
            id: self.id as i64,
            md5: self.hash.clone(),
            url: svc.post_url(self.id as i64),
            file: self.file_url(),
            sample: None,
            tags: self.tags(),
            score: self.score.unwrap_or(0) as i64,
            source: svc.id().to_string(),
            source_name: None,
            active: true,
            rating: 0,
            timestamp: self.change as i64,
        }
    }
}

#[derive(Deserialize)]
pub struct SafebooruTagResponse {
    pub tag: Vec<SafebooruTag>,
}

#[derive(Deserialize)]
pub struct SafebooruTag {
    pub id: i64,
    pub name: String,
    #[serde(rename = "type")]
    pub kind: i64,
}

impl SafebooruTag {
    pub fn to_tag(&self, svc: &str) -> CocoTag {
        CocoTag {
            id: self.id,
            name: self.name.clone(),
            service: svc.to_string(),
            kind: self.kind,
        }
    }
}
