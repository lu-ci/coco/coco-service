use crate::core::http::CocoHttpClient;
use crate::core::service::CocoCore;
use crate::core::utility::CocoUtility;
use crate::models::item::{CocoItem, CocoItemWrapper};
use crate::models::tag::CocoTag;
use crate::services::common::CocoService;
use log::{debug, error};
use serde::Deserialize;
use std::collections::HashMap;

#[derive(Default)]
pub struct YandereService;

#[async_trait::async_trait]
impl CocoService for YandereService {
    fn id(&self) -> &'static str {
        "yandere"
    }

    fn name(&self) -> &'static str {
        "Yande.re"
    }

    fn color(&self) -> u64 {
        0xad_3d_3d
    }

    fn api_url(&self) -> &'static str {
        "https://yande.re/post.json?limit=1000"
    }

    fn url_base(&self) -> &'static str {
        "https://yande.re/post/show"
    }

    fn icon_url(&self) -> &'static str {
        "https://i.imgur.com/KaL5NDw.png"
    }

    async fn collect(&self, core: &CocoCore) -> anyhow::Result<Vec<CocoItemWrapper>> {
        let mut items = Vec::new();
        let native_items = self.pull(core).await?;
        debug!(
            "Pulled {} native items from {}.",
            native_items.len(),
            self.name()
        );
        for ni in native_items {
            let item = ni.to_item(self);
            item.save(&core.db).await?;
            let wrapper = CocoItemWrapper::new(
                &item,
                self.make_embed(
                    &item,
                    // Some(item.describe(&core.db, self.id(), &item.tags).await?),
                    None,
                ),
            );
            items.push(wrapper);
        }
        Ok(items)
    }

    async fn collect_tags(&self) -> anyhow::Result<Vec<CocoTag>> {
        let mut tags = Vec::new();
        let uri = "https://konachan.com/tag.json?limit=0";
        let resp = CocoHttpClient::get(uri).await?;
        let body = resp.text().await?;
        let native_tags = serde_json::from_str::<Vec<YandereTag>>(&body)?;
        for nt in native_tags {
            tags.push(nt.to_tag(self.id()));
        }
        Ok(tags)
    }
}

impl YandereService {
    pub fn boxed() -> Box<dyn CocoService> {
        Box::new(Self::default())
    }

    pub async fn pull(&self, core: &CocoCore) -> anyhow::Result<Vec<YandereItem>> {
        let mut items = vec![];
        let mut item_map = HashMap::new();
        let queries = CocoUtility::get_tag_queries(core).await?;
        for query in queries {
            let mut page = 1;
            let mut finished = false;
            let length = query.split("+").collect::<Vec<&str>>().len();
            if length <= 6 {
                while !finished && page <= CocoUtility::max_pages() {
                    let uri = format!("{}&tags={}&page={}", self.api_url(), query, page);
                    debug!("Pulling: {}", &uri);
                    let resp = CocoHttpClient::get(&uri).await?;
                    let body = resp.text().await?;
                    let batch = match serde_json::from_str::<Vec<YandereItem>>(&body) {
                        Ok(batch) => batch,
                        Err(err) => {
                            error!("Failed to pull {}: {}!", self.name(), err);
                            vec![]
                        }
                    };
                    if !batch.is_empty() {
                        for bi in batch {
                            item_map.insert(bi.id, bi);
                        }
                        page += 1;
                    } else {
                        finished = true;
                    }
                }
            } else {
                debug!(
                    "Skipped this {} pull due to too many tags ({}): {}.",
                    self.name(),
                    length,
                    query
                );
            }
        }
        for (_, imi) in item_map {
            items.push(imi);
        }
        Ok(items)
    }
}

#[derive(Deserialize)]
pub struct YandereItem {
    pub id: u64,
    tags: String,
    pub md5: String,
    pub file_url: String,
    pub rating: String,
    pub created_at: u64,
    pub sample_url: String,
    status: String,
    score: u64,
    is_held: bool,
}

impl YandereItem {
    pub fn tags(&self) -> Vec<String> {
        let mut tags = Vec::new();
        for tag in self.tags.split(' ').collect::<Vec<&str>>() {
            tags.push(tag.to_string());
        }
        tags
    }

    pub fn to_item(&self, svc: &YandereService) -> CocoItem {
        CocoItem {
            id: self.id as i64,
            md5: self.md5.clone(),
            url: svc.post_url(self.id as i64),
            file: self.file_url.clone(),
            sample: Some(self.sample_url.clone()),
            tags: self.tags(),
            score: self.score as i64,
            source: svc.id().to_string(),
            source_name: None,
            active: self.status == "active" && !self.is_held,
            rating: CocoUtility::rating(&self.rating) as i64,
            timestamp: self.created_at as i64,
        }
    }
}

#[derive(Deserialize)]
pub struct YandereTag {
    pub id: i64,
    pub name: String,
    #[serde(rename = "type")]
    pub kind: i64,
}

impl YandereTag {
    pub fn to_tag(&self, svc: &str) -> CocoTag {
        CocoTag {
            id: self.id,
            name: self.name.clone(),
            service: svc.to_string(),
            kind: self.kind,
        }
    }
}
