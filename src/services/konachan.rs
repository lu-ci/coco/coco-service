use log::{debug, error};
use serde::Deserialize;
use std::collections::HashMap;

use crate::core::http::CocoHttpClient;
use crate::core::service::CocoCore;
use crate::core::utility::CocoUtility;
use crate::models::item::{CocoItem, CocoItemWrapper};
use crate::models::tag::CocoTag;
use crate::services::common::CocoService;

#[derive(Default)]
pub struct KonachanService;

#[async_trait::async_trait]
impl CocoService for KonachanService {
    fn id(&self) -> &'static str {
        "konachan"
    }

    fn name(&self) -> &'static str {
        "Konachan"
    }

    fn color(&self) -> u64 {
        0x47_3a_47
    }

    fn api_url(&self) -> &'static str {
        "https://konachan.com/post.json?limit=1000"
    }

    fn url_base(&self) -> &'static str {
        "https://konachan.com/post/show"
    }

    fn icon_url(&self) -> &'static str {
        "https://i.imgur.com/JJQ5OMm.png"
    }

    async fn collect(&self, core: &CocoCore) -> anyhow::Result<Vec<CocoItemWrapper>> {
        let mut items = Vec::new();
        let native_items = self.pull(core).await?;
        debug!(
            "Pulled {} native items from {}.",
            native_items.len(),
            self.name()
        );
        for ni in native_items {
            let item = ni.to_item(self);
            item.save(&core.db).await?;
            let wrapper = CocoItemWrapper::new(
                &item,
                self.make_embed(
                    &item,
                    // Some(item.describe(&core.db, self.id(), &item.tags).await?),
                    None,
                ),
            );
            items.push(wrapper);
        }
        Ok(items)
    }

    async fn collect_tags(&self) -> anyhow::Result<Vec<CocoTag>> {
        let mut tags = Vec::new();
        let uri = "https://konachan.com/tag.json?limit=0";
        let resp = CocoHttpClient::get(uri).await?;
        let body = resp.text().await?;
        let native_tags = serde_json::from_str::<Vec<KonachanTag>>(&body)?;
        for nt in native_tags {
            tags.push(nt.to_tag(self.id()));
        }
        Ok(tags)
    }
}

impl KonachanService {
    pub fn boxed() -> Box<dyn CocoService> {
        Box::new(Self::default())
    }

    pub async fn pull(&self, core: &CocoCore) -> anyhow::Result<Vec<KonachanItem>> {
        let mut items = vec![];
        let mut item_map = HashMap::new();
        let queries = CocoUtility::get_tag_queries(core).await?;
        for query in queries {
            let mut page = 1;
            let mut finished = false;
            let length = query.split("+").collect::<Vec<&str>>().len();
            if length <= 6 {
                while !finished && page <= CocoUtility::max_pages() {
                    let uri = format!("{}&tags={}&page={}", self.api_url(), query, page);
                    debug!("Pulling: {}", &uri);
                    let resp = CocoHttpClient::get(&uri).await?;
                    let body = resp.text().await?;
                    let batch = match serde_json::from_str::<Vec<KonachanItem>>(&body) {
                        Ok(batch) => batch,
                        Err(err) => {
                            error!("Failed to pull {}: {}!", self.name(), err);
                            vec![]
                        }
                    };
                    if !batch.is_empty() {
                        for bi in batch {
                            item_map.insert(bi.id, bi);
                        }
                        page += 1;
                    } else {
                        finished = true;
                    }
                }
            } else {
                debug!(
                    "Skipped this {} pull due to too many tags ({}): {}.",
                    self.name(),
                    length,
                    query
                );
            }
        }
        for (_, imi) in item_map {
            items.push(imi);
        }
        Ok(items)
    }
}

#[derive(Deserialize)]
pub struct KonachanItem {
    pub id: u64,
    pub tags: String,
    pub md5: String,
    pub file_url: String,
    pub sample_url: String,
    pub rating: String,
    pub created_at: u64,
    pub status: String,
    pub score: u64,
    pub is_held: bool,
}

impl KonachanItem {
    pub fn tags(&self) -> Vec<String> {
        let mut tags = Vec::new();
        for tag in self.tags.split(' ').collect::<Vec<&str>>() {
            tags.push(tag.to_string());
        }
        tags
    }

    pub fn to_item(&self, svc: &KonachanService) -> CocoItem {
        CocoItem {
            id: self.id as i64,
            md5: self.md5.clone(),
            url: svc.post_url(self.id as i64),
            file: self.file_url.clone(),
            sample: Some(self.sample_url.clone()),
            tags: self.tags(),
            score: self.score as i64,
            source: svc.id().to_string(),
            source_name: None,
            active: self.status == "active" && !self.is_held,
            rating: CocoUtility::rating(&self.rating) as i64,
            timestamp: self.created_at as i64,
        }
    }
}

#[derive(Deserialize)]
pub struct KonachanTag {
    pub id: i64,
    pub name: String,
    #[serde(rename = "type")]
    pub kind: i64,
}

impl KonachanTag {
    pub fn to_tag(&self, svc: &str) -> CocoTag {
        CocoTag {
            id: self.id,
            name: self.name.clone(),
            service: svc.to_string(),
            kind: self.kind,
        }
    }
}
