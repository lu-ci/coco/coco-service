use crate::core::database::CocoDatabase;
use bson::doc;
use serde::{Deserialize, Serialize};

const _MODEL_COLL: &str = "cooldowns";

#[derive(Serialize, Deserialize)]
pub struct CocoCooldown {
    pub key: String,
    pub category: String,
    pub timestamp: i64,
    pub duration: i64,
}

impl CocoCooldown {
    pub async fn _get(
        db: &CocoDatabase,
        category: &str,
        key: &str,
    ) -> anyhow::Result<Option<Self>> {
        let coll = db.coll(_MODEL_COLL);
        let search = doc! {"category": category, "key": key};
        Ok(if let Some(result) = coll.find_one(search, None).await? {
            Some(bson::from_document::<Self>(result)?)
        } else {
            None
        })
    }

    pub async fn _save(&self, db: &CocoDatabase) -> anyhow::Result<()> {
        let coll = db.coll(_MODEL_COLL);
        let search = doc! {"category": &self.category, "key": &self.key};
        let data = doc! {"$set": bson::to_bson(&self)?.as_document().unwrap().clone() };
        let opts: mongodb::options::UpdateOptions = mongodb::options::UpdateOptions::builder()
            .upsert(true)
            .build();
        let _ = coll.update_one(search, data, opts).await?;
        Ok(())
    }

    pub fn _expired(&self) -> bool {
        let now = chrono::Utc::now().timestamp();
        now >= self.timestamp + self.duration
    }
}
