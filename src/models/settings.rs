use std::ops::Deref;

use serde::{Deserialize, Serialize};

use crate::models::item::CocoItem;

#[derive(Clone, Debug, Default, Deserialize, Serialize)]
pub struct CocoRatings {
    #[serde(default = "bool::default")]
    pub safe: bool,
    #[serde(default = "bool::default")]
    pub questionable: bool,
    #[serde(default = "bool::default")]
    pub explicit: bool,
}

#[derive(Clone, Debug, Default, Deserialize, Serialize)]
pub struct CocoTags {
    #[serde(default = "Vec::<String>::default")]
    pub nsfw: Vec<String>,
    #[serde(default = "Vec::<String>::default")]
    pub exclude: Vec<String>,
    #[serde(default = "Vec::<String>::default")]
    pub require: Vec<String>,
    #[serde(default = "CocoRatings::default")]
    pub ratings: CocoRatings,
}

#[derive(Clone, Debug, Default, Deserialize, Serialize)]
pub struct CocoSites {
    #[serde(default = "bool::default")]
    pub yandere: bool,
    #[serde(default = "bool::default")]
    pub konachan: bool,
    #[serde(default = "bool::default")]
    pub safebooru: bool,
    #[serde(default = "bool::default")]
    pub reddit: bool,
}

#[derive(Clone, Debug, Default, Deserialize, Serialize)]
pub struct CocoSources {
    #[serde(default = "CocoSites::default")]
    pub sites: CocoSites,
    #[serde(default = "Vec::<String>::default")]
    pub subs: Vec<String>,
}

#[derive(Clone, Debug, Default, Deserialize, Serialize)]
pub struct CocoMargins {
    #[serde(default = "i64::default")]
    pub age: i64,
    #[serde(default = "i64::default")]
    pub score: i64,
    #[serde(default = "bool::default")]
    pub active: bool,
}

#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct CocoSettings {
    #[serde(default = "CocoTags::default")]
    pub tags: CocoTags,
    #[serde(default = "CocoSources::default")]
    pub sources: CocoSources,
    #[serde(default = "CocoMargins::default")]
    pub margins: CocoMargins,
    #[serde(default = "bool::default")]
    pub active: bool,
}

impl CocoSettings {
    fn has_required_tags(&self, tags: &[String]) -> bool {
        if self.tags.require.is_empty() {
            true
        } else {
            let mut valid = true;
            for tag in &self.tags.require {
                if !tags.contains(tag) {
                    valid = false;
                    break;
                }
            }
            valid
        }
    }

    fn has_excluded_tags(&self, tags: &[String]) -> bool {
        if self.tags.exclude.is_empty() {
            false
        } else {
            let mut excluded = false;
            for tag in &self.tags.exclude {
                if tags.contains(tag) {
                    excluded = true;
                    break;
                }
            }
            excluded
        }
    }

    fn has_nsfw_tags(&self, tags: &[String]) -> bool {
        if self.tags.nsfw.is_empty() {
            false
        } else {
            let mut nsfw = false;
            for tag in &self.tags.nsfw {
                if tags.contains(tag) {
                    nsfw = true;
                    break;
                }
            }
            nsfw
        }
    }

    fn is_active_service(&self, item: &CocoItem) -> bool {
        let mut lows = Vec::new();
        for source in &self.sources.subs {
            lows.push(source.to_lowercase());
        }
        match item.source.deref() {
            "konachan" => self.sources.sites.konachan,
            "yandere" => self.sources.sites.yandere,
            "reddit" => {
                if let Some(sid) = &item.source_name {
                    lows.contains(&sid.to_lowercase())
                } else {
                    false
                }
            }
            "safebooru" => self.sources.sites.safebooru,
            _ => false,
        }
    }

    fn is_active_margin(&self, item: &CocoItem) -> bool {
        let now = chrono::Utc::now().timestamp();
        let age = now >= item.timestamp + self.margins.age;
        let score = item.score >= self.margins.score;
        age && score && item.active
    }

    fn is_active_rating(&self, item: &CocoItem) -> bool {
        match item.rating {
            0 => self.tags.ratings.safe,
            1 => self.tags.ratings.questionable,
            2 => self.tags.ratings.explicit,
            _ => false,
        }
    }

    pub fn nsfw(&self, item: &CocoItem) -> bool {
        match item.rating {
            0 => false,
            1 => self.has_nsfw_tags(&item.tags),
            2 => true,
            _ => true,
        }
    }

    pub fn validate(&self, item: &CocoItem) -> bool {
        self.has_required_tags(&item.tags)
            && !self.has_excluded_tags(&item.tags)
            && self.is_active_service(item)
            && self.is_active_margin(item)
            && self.is_active_rating(item)
    }
}
