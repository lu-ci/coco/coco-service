use crate::core::database::CocoDatabase;
use crate::core::service::CocoCore;
use crate::models::tag::{CocoTag, CocoTagKind};
use bson::doc;
use heck::ToTitleCase;
// use log::debug;
use serde::{Deserialize, Serialize};
use serde_json::Value;
use serenity::model::webhook::Webhook;
use tokio::time::Duration;

const MODEL_COLL: &str = "posts";

#[derive(Clone, Deserialize, Serialize)]
pub struct CocoItem {
    pub id: i64,
    pub md5: String,
    pub url: String,
    pub file: String,
    #[serde(default = "Option::<String>::default")]
    pub sample: Option<String>,
    pub tags: Vec<String>,
    pub score: i64,
    pub source: String,
    #[serde(default = "Option::<String>::default")]
    pub source_name: Option<String>,
    pub active: bool,
    pub rating: i64,
    pub timestamp: i64,
}

impl CocoItem {
    pub async fn save(&self, db: &CocoDatabase) -> anyhow::Result<()> {
        let coll = db.coll(MODEL_COLL);
        let search = doc! {"id": &self.id};
        let data = doc! {"$set": bson::to_bson(&self)?.as_document().unwrap().clone() };
        let opts: mongodb::options::UpdateOptions = mongodb::options::UpdateOptions::builder()
            .upsert(true)
            .build();
        let _ = coll.update_one(search, data, opts).await?;
        Ok(())
    }

    pub async fn _describe(
        &self,
        db: &CocoDatabase,
        service: &str,
        tags: &Vec<String>,
    ) -> anyhow::Result<String> {
        let mut characters = Vec::new();
        let mut artists = Vec::new();
        let mut franchises = Vec::new();

        for tag in tags {
            if let Some(tag) = CocoTag::_get(db, tag, &service).await? {
                let name = tag.name.replace('_', " ").to_title_case();
                match tag._kind() {
                    CocoTagKind::Character => characters.push(name),
                    CocoTagKind::Artist => artists.push(name),
                    CocoTagKind::Copyright => franchises.push(name),
                    _ => {}
                }
            }
        }
        let mut description = String::new();
        if !characters.is_empty() {
            description += &format!("**{}**", characters.join(", "));
        }
        if !franchises.is_empty() {
            if description.is_empty() {
                description += "From "
            } else {
                description += " from "
            }
            description += &format!("**{}**", franchises.join(", "));
        }
        if !artists.is_empty() {
            if description.is_empty() {
                description += "By "
            } else {
                description += " by "
            }
            description += &format!("**{}**", artists.join(", "))
        }
        if !description.is_empty() {
            description += ".";
            // debug!("{}: {} ({})", self.id, &self.url, &description);
        }
        Ok(description)
    }
}

pub struct CocoItemWrapper {
    pub item: CocoItem,
    pub embed: Value,
}

impl CocoItemWrapper {
    pub fn new(item: &CocoItem, embed: Value) -> Self {
        Self {
            item: item.clone(),
            embed,
        }
    }

    pub async fn send(&self, core: &CocoCore, hook: &Webhook) -> anyhow::Result<()> {
        tokio::time::sleep(Duration::from_secs(core.cfg.core.sleep)).await;
        if !core.cfg.core.debug {
            hook.execute(&core.http, true, |w| {
                w.embeds(vec![self.embed.clone()]);
                w
            })
            .await?;
        }
        Ok(())
    }
}
