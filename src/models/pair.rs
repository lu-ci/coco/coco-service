use bson::{doc, Document};
use log::{error, info, warn};
use serde::{Deserialize, Serialize};
use serenity::futures::TryStreamExt;
use serenity::model::webhook::Webhook;

use crate::core::database::CocoDatabase;
use crate::core::service::CocoCore;
use crate::models::channel::CocoChannel;
use crate::models::item::CocoItemWrapper;
use crate::models::settings::CocoSettings;
use crate::models::track::CocoTrack;

const MODEL_COLL: &str = "pairs";

#[derive(Deserialize, Serialize)]
pub struct CocoChannelPair {
    pub id: i64,
    pub sfw: Option<i64>,
    pub nsfw: Option<i64>,
    pub name: String,
    pub settings: Option<CocoSettings>,
}

impl CocoChannelPair {
    pub async fn _get(db: &CocoDatabase, id: i64) -> anyhow::Result<Option<Self>> {
        let coll = db.coll(MODEL_COLL);
        let search = doc! {"id": id};
        Ok(if let Some(doc) = coll.find_one(search, None).await? {
            Some(bson::from_document::<Self>(doc)?)
        } else {
            None
        })
    }

    pub async fn _save(&self, db: &CocoDatabase) -> anyhow::Result<()> {
        let coll = db.coll(MODEL_COLL);
        let search = doc! {"id": &self.id};
        let data = doc! {"$set": bson::to_bson(&self)?.as_document().unwrap().clone() };
        let opts: mongodb::options::UpdateOptions = mongodb::options::UpdateOptions::builder()
            .upsert(true)
            .build();
        let _ = coll.update_one(search, data, opts).await?;
        Ok(())
    }

    pub async fn _deactivate(&self, db: &CocoDatabase) -> anyhow::Result<()> {
        if let Some(pair) = Self::_get(db, self.id).await? {
            if pair.settings.is_some() {
                let coll = db.coll(MODEL_COLL);
                let search = doc! {"id": &self.id};
                let data = doc! {"$set": doc! {"settings.active": false} };
                let opts: mongodb::options::UpdateOptions =
                    mongodb::options::UpdateOptions::builder()
                        .upsert(true)
                        .build();
                let _ = coll.update_one(search, data, opts).await?;
                warn!("Deactivated pair {} due to a failed send.", pair.id);
            }
        }
        Ok(())
    }

    pub async fn active_service(db: &CocoDatabase, name: &str) -> anyhow::Result<bool> {
        let coll = db.coll(MODEL_COLL);
        let key = format!("settings.sources.sites.{}", name);
        let search = doc! {key: true};
        let active = coll.count_documents(search, None).await?;
        Ok(active != 0)
    }

    pub async fn fetch_active(db: &CocoDatabase) -> anyhow::Result<Vec<Self>> {
        let mut pairs = Vec::new();
        let coll = db.coll(MODEL_COLL);
        let search = doc! {"settings.active": true};
        let cursor = coll.find(search, None).await?;
        let results = cursor.try_collect::<Vec<Document>>().await?;
        for result in results {
            let pair = bson::from_document::<Self>(result)?;
            pairs.push(pair);
        }
        Ok(pairs)
    }

    async fn fetch_channel(
        db: &CocoDatabase,
        id: Option<i64>,
    ) -> anyhow::Result<Option<CocoChannel>> {
        Ok(if let Some(id) = id {
            CocoChannel::get(db, id).await?
        } else {
            None
        })
    }

    pub async fn sfw_channel(&self, db: &CocoDatabase) -> anyhow::Result<Option<CocoChannel>> {
        Self::fetch_channel(db, self.sfw).await
    }

    pub async fn nsfw_channel(&self, db: &CocoDatabase) -> anyhow::Result<Option<CocoChannel>> {
        Self::fetch_channel(db, self.nsfw).await
    }

    async fn fetch_hook(
        core: &CocoCore,
        chn: Option<CocoChannel>,
    ) -> anyhow::Result<Option<Webhook>> {
        Ok(if let Some(chn) = chn {
            Some(
                core.http
                    .get_webhook_with_token(chn.id as u64, &chn.token)
                    .await?,
            )
        } else {
            None
        })
    }

    pub async fn sfw_hook(&self, core: &CocoCore) -> anyhow::Result<Option<Webhook>> {
        Self::fetch_hook(core, self.sfw_channel(&core.db).await?).await
    }

    pub async fn nsfw_hook(&self, core: &CocoCore) -> anyhow::Result<Option<Webhook>> {
        Self::fetch_hook(core, self.nsfw_channel(&core.db).await?).await
    }

    pub async fn process(
        &self,
        core: &mut CocoCore,
        items: &[CocoItemWrapper],
    ) -> anyhow::Result<()> {
        if let Some(settings) = &self.settings {
            let sfw_hook = self.sfw_hook(core).await?;
            let nsfw_hook = self.nsfw_hook(core).await?;
            for item in items {
                if !CocoTrack::exists(&mut core.db, self.id, &item.item.md5).await?
                    && settings.validate(&item.item)
                {
                    let nsfw = settings.nsfw(&item.item);
                    if let Some(target) = if nsfw { &nsfw_hook } else { &sfw_hook } {
                        let info = format!(
                            "ID: {} | SVC: {} | PAIR: {} | NSFW: {} | CHN: {}",
                            item.item.id, &item.item.source, self.id, nsfw, target.id
                        );
                        match self.send(core, item, target).await {
                            Ok(_) => {
                                info!("{}", info);
                            }
                            Err(err) => {
                                error!("{} | ERR: {}", info, err);
                                // self.deactivate(&core.db).await?;
                                break;
                            }
                        }
                    }
                }
            }
        }
        Ok(())
    }

    async fn send(
        &self,
        core: &mut CocoCore,
        item: &CocoItemWrapper,
        target: &Webhook,
    ) -> anyhow::Result<()> {
        let track = CocoTrack::new(item.item.id, self.id, &item.item.md5, &item.item.source);
        track.save(&mut core.db).await?;
        item.send(core, target).await?;
        Ok(())
    }
}
