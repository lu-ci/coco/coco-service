use crate::core::database::CocoDatabase;
use bson::{doc, Document};
use serde::{Deserialize, Serialize};
use serenity::futures::TryStreamExt;

const _MODEL_COLL: &str = "tags";

pub enum CocoTagKind {
    General,
    Artist,
    Copyright,
    Character,
    Unknown,
}

impl From<&CocoTag> for CocoTagKind {
    fn from(tag: &CocoTag) -> Self {
        match tag.kind {
            0 => Self::General,
            1 => Self::Artist,
            3 => Self::Copyright,
            4 => Self::Character,
            _ => Self::Unknown,
        }
    }
}

#[derive(Serialize, Deserialize)]
pub struct CocoTag {
    pub id: i64,
    pub name: String,
    pub service: String,
    pub kind: i64,
}

impl CocoTag {
    pub fn _kind(&self) -> CocoTagKind {
        CocoTagKind::from(self)
    }

    pub async fn _get(
        db: &CocoDatabase,
        name: &str,
        service: &str,
    ) -> anyhow::Result<Option<Self>> {
        let key = format!("{}:{}", &service, &name);
        Ok(if let Some(cached) = db.cache.get(&key) {
            serde_json::from_str(cached)?
        } else {
            // let coll = db.coll(MODEL_COLL);
            // let search = doc! {"name": name, "service": service};
            // if let Some(result) = coll.find_one(search, None).await? {
            //     let tag = bson::from_document::<Self>(result)?;
            //     Some(tag)
            // } else {
            //     None
            // }
            None
        })
    }

    pub async fn _ids(db: &CocoDatabase, service: &str) -> anyhow::Result<Vec<i64>> {
        let mut ids = Vec::new();
        let coll = db.coll(_MODEL_COLL);
        let search = doc! {"service": service};
        let cursor = coll.find(search, None).await?;
        let documents = cursor.try_collect::<Vec<Document>>().await?;
        for doc in documents {
            let tag = bson::from_document::<Self>(doc)?;
            ids.push(tag.id);
        }
        Ok(ids)
    }

    pub async fn _precache(db: &mut CocoDatabase) -> anyhow::Result<usize> {
        let mut count = 0;
        let coll = db.coll(_MODEL_COLL);
        let cursor = coll.find(None, None).await?;
        let documents = cursor.try_collect::<Vec<Document>>().await?;
        for doc in documents {
            let tag = bson::from_document::<Self>(doc)?;
            let key = format!("{}:{}", &tag.service, &tag.name);
            db.cache.insert(key, serde_json::to_string(&tag)?);
            count += 1;
        }
        Ok(count)
    }

    pub async fn _bulk(db: &mut CocoDatabase, tags: Vec<Self>) -> anyhow::Result<()> {
        let coll = db.coll(_MODEL_COLL);
        let mut docs = Vec::new();
        for tag in tags {
            docs.push(bson::to_document(&tag)?);
        }
        let _ = coll.insert_many(docs, None).await?;
        Ok(())
    }

    pub async fn _save(&self, db: &CocoDatabase) -> anyhow::Result<()> {
        let coll = db.coll(_MODEL_COLL);
        let search = doc! {"id": &self.id, "service": &self.service};
        let data = doc! {"$set": bson::to_bson(&self)?.as_document().unwrap().clone() };
        let opts: mongodb::options::UpdateOptions = mongodb::options::UpdateOptions::builder()
            .upsert(true)
            .build();
        let _ = coll.update_one(search, data, opts).await?;
        Ok(())
    }
}
