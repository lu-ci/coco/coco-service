use crate::core::database::CocoDatabase;
use bson::doc;
use serde::{Deserialize, Serialize};

const MODEL_COLL: &str = "hashes";

#[derive(Deserialize, Serialize)]
pub struct CocoHash {
    pub id: i64,
    pub service: String,
    pub hash: String,
}

impl CocoHash {
    pub fn new(id: i64, service: impl ToString, hash: impl ToString) -> Self {
        let service = service.to_string();
        let hash = hash.to_string();
        Self { id, service, hash }
    }

    pub async fn save(&self, db: &CocoDatabase) -> anyhow::Result<()> {
        let coll = db.coll(MODEL_COLL);
        let search = doc! {"id": &self.id, "service": &self.service};
        let data = doc! {"$set": bson::to_bson(&self)?.as_document().unwrap().clone() };
        let opts: mongodb::options::UpdateOptions = mongodb::options::UpdateOptions::builder()
            .upsert(true)
            .build();
        let _ = coll.update_one(search, data, opts).await?;
        Ok(())
    }

    pub async fn find(
        db: &CocoDatabase,
        id: i64,
        service: impl ToString,
    ) -> anyhow::Result<Option<Self>> {
        let service = service.to_string();
        let coll = db.coll(MODEL_COLL);
        let search = doc! {"id": id, "service": service};
        Ok(if let Some(doc) = coll.find_one(search, None).await? {
            Some(bson::from_document::<Self>(doc)?)
        } else {
            None
        })
    }
}
