use bson::doc;
use serde::{Deserialize, Serialize};

use crate::core::database::CocoDatabase;

const MODEL_COLL: &str = "channels";

#[derive(Debug, Deserialize, Serialize, PartialEq)]
pub struct CocoChannel {
    pub id: i64,
    pub token: String,
}

impl CocoChannel {
    pub async fn get(db: &CocoDatabase, id: i64) -> anyhow::Result<Option<Self>> {
        let coll = db.coll(MODEL_COLL);
        let search = doc! {"id": id};
        Ok(if let Some(doc) = coll.find_one(search, None).await? {
            Some(bson::from_document::<Self>(doc)?)
        } else {
            None
        })
    }
}
