use crate::core::database::CocoDatabase;
use bson::{doc, Document};
use log::info;
use mongodb::options::FindOptions;
use serde::{Deserialize, Serialize};
use serenity::futures::TryStreamExt;

pub const MODEL_COLL: &str = "tracks";

#[derive(Deserialize, Serialize)]
pub struct CocoTrack {
    pub post: i64,
    pub pair: i64,
    pub hash: String,
    pub service: String,
    pub timestamp: i64,
}

impl CocoTrack {
    pub fn new(post: i64, pair: i64, hash: impl ToString, service: impl ToString) -> Self {
        let hash = hash.to_string();
        let service = service.to_string();
        Self {
            post,
            pair,
            hash,
            service,
            timestamp: chrono::Utc::now().timestamp(),
        }
    }

    pub async fn _find(db: &CocoDatabase, pair: i64) -> anyhow::Result<Vec<Self>> {
        let mut tracks = Vec::new();
        let coll = db.coll(MODEL_COLL);
        let search = doc! {"pair": pair};
        let opts = FindOptions::builder().sort(doc! {"timestamp": -1}).build();
        let cursor = coll.find(search, opts).await?;
        let results = cursor.try_collect::<Vec<Document>>().await?;
        for result in results {
            let track = bson::from_document::<Self>(result)?;
            tracks.push(track);
        }
        Ok(tracks)
    }

    pub async fn precache(db: &mut CocoDatabase) -> anyhow::Result<()> {
        info!("Precaching tracks...");
        let mut count = 0;
        let now = chrono::Utc::now().timestamp();
        let filter = doc! {"timestamp": {"$gt": now - (3600 * 24)}};
        let coll = db.coll(MODEL_COLL);
        let cursor = coll.find(filter, None).await?;
        let results = cursor.try_collect::<Vec<Document>>().await?;
        for result in results {
            let track = bson::from_document::<Self>(result)?;
            Self::save_in_cache(db, track.pair, &track.hash)?;
            count += 1;
        }
        info!("Precached {} tracks.", count);
        Ok(())
    }

    pub fn exists_in_cache(
        db: &CocoDatabase,
        pair: i64,
        hash: impl ToString,
    ) -> anyhow::Result<bool> {
        let hash = hash.to_string();
        let key = format!("{}", pair);
        let exists = if let Some(search) = db.cache.get(&key) {
            let ids = serde_json::from_str::<Vec<String>>(search)?;
            ids.contains(&hash)
        } else {
            false
        };
        Ok(exists)
    }

    pub fn save_in_cache(
        db: &mut CocoDatabase,
        pair: i64,
        hash: impl ToString,
    ) -> anyhow::Result<()> {
        let hash = hash.to_string();
        let key = format!("{}", pair);
        let mut ids = if let Some(search) = db.cache.get(&key) {
            serde_json::from_str::<Vec<String>>(search)?
        } else {
            Vec::new()
        };
        if !ids.contains(&hash) {
            ids.push(hash);
        }
        db.cache.insert(key, serde_json::to_string(&ids)?);
        Ok(())
    }

    pub async fn exists_in_db(
        db: &CocoDatabase,
        pair: i64,
        hash: impl ToString,
    ) -> anyhow::Result<bool> {
        let hash = hash.to_string();
        let coll = db.coll(MODEL_COLL);
        let search = doc! {"pair": pair, "hash": hash};
        Ok(coll.count_documents(search, None).await? != 0)
    }

    pub async fn save_in_db(&self, db: &CocoDatabase) -> anyhow::Result<()> {
        let coll = db.coll(MODEL_COLL);
        let data = bson::to_bson(&self)?.as_document().unwrap().clone();
        let _ = coll.insert_one(data, None).await?;
        Ok(())
    }

    pub async fn exists(
        db: &mut CocoDatabase,
        pair: i64,
        hash: impl ToString,
    ) -> anyhow::Result<bool> {
        let hash = hash.to_string();
        Ok(if !Self::exists_in_cache(db, pair, &hash)? {
            let exists = Self::exists_in_db(db, pair, &hash).await?;
            if exists {
                Self::save_in_cache(db, pair, &hash)?;
            }
            exists
        } else {
            true
        })
    }

    pub async fn save(&self, db: &mut CocoDatabase) -> anyhow::Result<()> {
        self.save_in_db(db).await?;
        Self::save_in_cache(db, self.pair, &self.hash)?;
        Ok(())
    }
}
