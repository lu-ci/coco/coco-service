use crate::core::config::CocoConfig;
use fern::colors::{Color, ColoredLevelConfig};
use log::info;
use log::LevelFilter;

pub struct CocoLogger;

impl CocoLogger {
    pub fn init(cfg: &CocoConfig) -> anyhow::Result<()> {
        let colors = ColoredLevelConfig::default()
            .info(Color::Cyan)
            .warn(Color::BrightYellow)
            .error(Color::BrightRed)
            .debug(Color::BrightMagenta);
        fern::Dispatch::new()
            .format(move |out, msg, rcd| {
                out.finish(format_args!(
                    "{} | {} | {} | {}",
                    chrono::Local::now().format("%Y-%m-%d %H:%M:%S"),
                    colors.color(rcd.level()),
                    rcd.target(),
                    msg
                ))
            })
            .level(if cfg.core.debug {
                LevelFilter::Debug
            } else {
                LevelFilter::Info
            })
            .level_for("sqlx", LevelFilter::Warn)
            .level_for("serenity", LevelFilter::Warn)
            .level_for("reqwest", LevelFilter::Warn)
            .level_for("hyper", LevelFilter::Warn)
            .level_for("serde_xml_rs", LevelFilter::Warn)
            .chain(std::io::stdout())
            .apply()?;
        info!("Logger initialized.");
        Ok(())
    }
}
