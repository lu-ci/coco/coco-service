use serde::Deserialize;

#[derive(Clone)]
pub struct CocoConfig {
    pub core: CocoCoreConfig,
    pub db: CocoDatabaseConfig,
    pub reddit: CocoRedditConfig,
}

impl CocoConfig {
    pub fn new() -> anyhow::Result<Self> {
        Ok(Self {
            core: CocoCoreConfig::new()?,
            db: CocoDatabaseConfig::new()?,
            reddit: CocoRedditConfig::new()?,
        })
    }
}

#[derive(Clone, Deserialize)]
pub struct CocoCoreConfig {
    pub debug: bool,
    pub sleep: u64,
    pub interval: u64,
}

impl Default for CocoCoreConfig {
    fn default() -> Self {
        Self {
            debug: false,
            sleep: 5,
            interval: 60,
        }
    }
}

impl CocoCoreConfig {
    pub fn new() -> anyhow::Result<Self> {
        let path = std::path::Path::new("config/core.yml");
        if path.exists() {
            let content = std::fs::read_to_string(&path)?;
            let cfg = serde_yaml::from_str::<Self>(&content)?;
            Ok(cfg)
        } else {
            Ok(Self::default())
        }
    }
}

#[derive(Clone, Deserialize)]
pub struct CocoDatabaseConfig {
    pub host: String,
    pub port: u16,
    pub username: Option<String>,
    pub password: Option<String>,
    pub database: String,
}

impl Default for CocoDatabaseConfig {
    fn default() -> Self {
        Self {
            host: "127.0.0.1".to_string(),
            port: 27017,
            username: None,
            password: None,
            database: "coco".to_string(),
        }
    }
}

impl CocoDatabaseConfig {
    pub fn new() -> anyhow::Result<Self> {
        let path = std::path::Path::new("config/database.yml");
        if path.exists() {
            let content = std::fs::read_to_string(&path)?;
            let cfg = serde_yaml::from_str::<Self>(&content)?;
            Ok(cfg)
        } else {
            Ok(Self::default())
        }
    }

    pub fn get_uri(&self) -> String {
        if self.username.is_some() && self.password.is_some() {
            let username = self.username.clone().unwrap();
            let password = self.password.clone().unwrap();
            format!(
                "mongodb://{}:{}@{}:{}/",
                username, password, &self.host, self.port
            )
        } else {
            format!("mongodb://{}:{}/", &self.host, self.port)
        }
    }
}

#[derive(Clone, Default, Deserialize)]
pub struct CocoRedditConfig {
    pub id: String,
    pub secret: String,
}

impl CocoRedditConfig {
    pub fn new() -> anyhow::Result<Self> {
        let path = std::path::Path::new("config/reddit.yml");
        if path.exists() {
            let content = std::fs::read_to_string(&path)?;
            let cfg = serde_yaml::from_str::<Self>(&content)?;
            Ok(cfg)
        } else {
            Ok(Self::default())
        }
    }
}
