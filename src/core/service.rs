use crate::core::config::CocoConfig;
use crate::core::database::CocoDatabase;
use crate::core::logging::CocoLogger;
use crate::core::reddit::core::RedditCore;
use crate::models::cooldown::CocoCooldown;
use crate::models::item::CocoItemWrapper;
use crate::models::pair::CocoChannelPair;
use crate::models::tag::CocoTag;
use crate::services::common::CocoService;
use crate::services::konachan::KonachanService;
use crate::services::reddit::RedditService;
use crate::services::safebooru::SafebooruService;
use crate::services::yandere::YandereService;
use log::{error, info};
use serenity::http::Http;

pub struct CocoCore {
    pub cfg: CocoConfig,
    pub db: CocoDatabase,
    pub http: Http,
    pub reddit: RedditCore,
    services: Vec<Box<dyn CocoService>>,
}

impl CocoCore {
    pub async fn new() -> anyhow::Result<Self> {
        let cfg = CocoConfig::new()?;
        CocoLogger::init(&cfg)?;
        let db = CocoDatabase::new(&cfg).await?;
        let http = Http::default();
        let services = vec![
            KonachanService::boxed(),
            SafebooruService::boxed(),
            YandereService::boxed(),
            RedditService::boxed(),
        ];
        let reddit = RedditCore::new(&cfg.reddit).await?;
        let core = Self {
            cfg,
            db,
            http,
            reddit,
            services,
        };
        Ok(core)
    }

    fn estimate(pairs: &[CocoChannelPair], items: &[CocoItemWrapper]) -> u64 {
        let mut count = 0;
        for pair in pairs {
            if let Some(settings) = &pair.settings {
                for item in items {
                    if settings.validate(&item.item) {
                        count += 1;
                    }
                }
            }
        }
        count
    }

    pub async fn _init(&mut self) -> anyhow::Result<()> {
        let mut total = 0;
        for service in &self.services {
            let ready = if let Some(cd) = CocoCooldown::_get(&self.db, "tags", service.id()).await?
            {
                cd._expired()
            } else {
                true
            };
            if ready {
                info!("Collecting tags from {}...", service.name());
                let tags = service.collect_tags().await?;
                info!("Collected {} tags from {}.", tags.len(), service.name());
                total += tags.len();
                let mut clean = Vec::new();
                let ids = CocoTag::_ids(&self.db, service.id()).await?;
                for tag in tags {
                    if !ids.contains(&tag.id) {
                        clean.push(tag);
                    }
                }
                if !clean.is_empty() {
                    info!("Saving {} tags for {}...", clean.len(), service.name());
                    CocoTag::_bulk(&mut self.db, clean).await?;
                } else {
                    info!("Nothing to save, no new tags for {}.", service.name());
                }
                let cd = CocoCooldown {
                    key: service.id().to_string(),
                    category: "tags".to_string(),
                    timestamp: chrono::Utc::now().timestamp(),
                    duration: 24 * 3600,
                };
                cd._save(&self.db).await?;
            }
        }
        if total > 0 {
            info!("Done collecting {} tags...", total);
        }
        info!("Precaching all the tags...");
        let count = CocoTag::_precache(&mut self.db).await?;
        info!("Precached {} tags.", count);
        Ok(())
    }

    pub async fn run(&mut self) -> anyhow::Result<()> {
        if self.reddit.auth.expired() {
            self.reddit.refresh().await?;
        }
        let mut items = Vec::new();
        let pairs = CocoChannelPair::fetch_active(&self.db).await?;
        info!(
            "Prepared {} active pair{}.",
            pairs.len(),
            if pairs.len() == 1 { "" } else { "s" }
        );
        if !pairs.is_empty() {
            for service in &self.services {
                if service.is_active(&self.db).await? {
                    info!("Collecting from {}...", service.name());
                    match service.collect(self).await {
                        Ok(wrappers) => {
                            info!(
                                "Collected {} items from {}.",
                                wrappers.len(),
                                service.name()
                            );
                            items.extend(wrappers);
                        }
                        Err(err) => {
                            error!("Failed to collect from {}: {}!", service.name(), err);
                        }
                    }
                } else {
                    info!("Skipping {} cause no pair uses it...", service.name());
                }
            }
        }
        items.sort_by(|a, b| a.item.timestamp.cmp(&b.item.timestamp));
        info!(
            "Estimating {} valid posts to be sent without checking tracks.",
            Self::estimate(&pairs, &items)
        );
        for pair in &pairs {
            info!("Processing items for pair {}...", &pair.id);
            if let Err(err) = pair.process(self, &items).await {
                error!("Failed to process items for pair {}: {}!", pair.id, err);
            }
        }
        Ok(())
    }
}
