use crate::core::config::{CocoConfig, CocoDatabaseConfig};
use crate::models::track::CocoTrack;
use std::collections::HashMap;

#[derive(Clone)]
pub struct CocoDatabase {
    pub cfg: CocoDatabaseConfig,
    pub cli: mongodb::Client,
    pub cache: HashMap<String, String>,
}

impl CocoDatabase {
    pub async fn new(cfg: &CocoConfig) -> anyhow::Result<Self> {
        let opts = mongodb::options::ClientOptions::parse(cfg.db.get_uri()).await?;
        let cli = mongodb::Client::with_options(opts)?;
        let mut db = Self {
            cfg: cfg.db.clone(),
            cli,
            cache: HashMap::new(),
        };
        CocoTrack::precache(&mut db).await?;
        Ok(db)
    }

    pub fn db(&self) -> mongodb::Database {
        self.cli.database(&self.cfg.database)
    }

    pub fn coll(&self, name: impl ToString) -> mongodb::Collection<bson::Document> {
        let name = name.to_string();
        self.db().collection(&name)
    }
}
