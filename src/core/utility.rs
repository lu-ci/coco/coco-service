use crate::models::pair::CocoChannelPair;
use crate::models::settings::CocoTags;
use crate::CocoCore;
use std::collections::hash_map::DefaultHasher;
use std::hash::Hasher;
use std::ops::Add;

const MAX_PAGES: usize = 1;

pub struct CocoUtility;

impl CocoUtility {
    pub fn rating(rating: &str) -> u64 {
        match rating {
            "s" => 0,
            "q" => 1,
            "e" => 2,
            _ => 1,
        }
    }

    pub fn hash(text: &str) -> u64 {
        let mut hasher = DefaultHasher::new();
        hasher.write(text.as_bytes());
        hasher.finish()
    }

    fn _are_tags_nsfw(nsfw_tags: Vec<String>, tags: Vec<String>) -> bool {
        let mut nsfw = false;
        for tag in &nsfw_tags {
            if tags.contains(tag) {
                nsfw = true;
                break;
            }
        }
        nsfw
    }

    fn clean_tag(tag: &String) -> String {
        return tag.to_lowercase().replace(' ', "_");
    }

    fn clean_tags(tags: &Vec<String>) -> Vec<String> {
        let mut new = vec![];
        for tag in tags {
            new.push(Self::clean_tag(tag));
        }
        new
    }

    fn get_tag_query(tags: &CocoTags, mini: bool) -> String {
        let mut lookup = String::new();
        if !tags.require.is_empty() {
            let required = Self::clean_tags(&tags.require);
            let positive = required.join("+");
            lookup = lookup.add(&positive);
        }
        if !tags.exclude.is_empty() && !mini {
            let excluded = Self::clean_tags(&tags.exclude);
            let negative = excluded.join("+-");
            let negative_str = format!("-{}", negative);
            lookup = lookup.add(&negative_str);
        }
        lookup
    }

    pub async fn get_tag_queries(core: &CocoCore) -> anyhow::Result<Vec<String>> {
        let mut queries = vec![];
        let pairs = CocoChannelPair::fetch_active(&core.db).await?;
        for pair in pairs {
            if let Some(settings) = pair.settings {
                queries.push(Self::get_tag_query(&settings.tags, false));
                queries.push(Self::get_tag_query(&settings.tags, true));
            }
        }
        Ok(queries)
    }

    pub fn max_pages() -> usize {
        MAX_PAGES
    }
}
