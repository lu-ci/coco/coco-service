use crate::core::config::CocoRedditConfig;
use reqwest::{Client, Response};
use serde::Deserialize;

// Treat active auth as expired if only this many seconds are left.
const EXPIRATION_OFFSET: i64 = 300;
const REDDIT_USER_AGENT: &str = "cli:coco-service:0.1.0 (by /u/axaz0r)";
const TOKEN_URI: &str = "https://www.reddit.com/api/v1/access_token";
const TOKEN_SCOPE: &str = "https://oauth.reddit.com/grants/installed_client";
const DEVICE_ID: &str = "LUCI_COCO_SERVICE_REDDIT";

#[derive(Deserialize)]
pub struct RedditAuth {
    pub access_token: String,
    pub token_type: String,
    pub expires_in: i64,
    pub scope: String,
    #[serde(default = "RedditAuth::now")]
    pub created_at: i64,
}

impl RedditAuth {
    fn now() -> i64 {
        chrono::Utc::now().timestamp()
    }

    pub async fn new(cfg: &CocoRedditConfig) -> anyhow::Result<Self> {
        let client = RedditCore::client()?;
        let form = format!("grant_type={}&device_id={}", TOKEN_SCOPE, DEVICE_ID);
        let resp = client
            .post(TOKEN_URI)
            .basic_auth(&cfg.id, Some(&cfg.secret))
            .body(form)
            .send()
            .await?;
        let body = resp.text().await?;
        Ok(serde_json::from_str::<Self>(&body)?)
    }

    pub fn expired(&self) -> bool {
        chrono::Utc::now().timestamp() > (self.created_at + self.expires_in - EXPIRATION_OFFSET)
    }
}

pub struct RedditCore {
    pub cfg: CocoRedditConfig,
    pub auth: RedditAuth,
}

impl RedditCore {
    pub async fn new(cfg: &CocoRedditConfig) -> anyhow::Result<Self> {
        let cfg = cfg.clone();
        let auth = RedditAuth::new(&cfg).await?;
        Ok(Self { cfg, auth })
    }

    pub async fn refresh(&mut self) -> anyhow::Result<()> {
        self.auth = RedditAuth::new(&self.cfg).await?;
        Ok(())
    }

    pub fn client() -> anyhow::Result<Client> {
        Ok(Client::builder().user_agent(REDDIT_USER_AGENT).build()?)
    }

    pub async fn get(&self, url: &str) -> anyhow::Result<Response> {
        let client = Self::client()?;
        let resp = client
            .get(url)
            .bearer_auth(&self.auth.access_token)
            .send()
            .await?;
        Ok(resp)
    }
}
