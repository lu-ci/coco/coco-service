use crate::core::service::CocoCore;
use log::error;
use serde::Deserialize;

const IMG_EXTS: [&str; 5] = ["png", "jpg", "gif", "jpeg", "webp"];

pub struct RedditController {
    sub: String,
    responses: Vec<RedditResponse>,
}

impl RedditController {
    pub fn new(sub: impl ToString) -> Self {
        Self {
            sub: sub.to_string(),
            responses: Vec::new(),
        }
    }

    pub fn url(&self) -> String {
        format!("https://oauth.reddit.com/r/{}.json?limit=100", self.sub)
    }

    pub fn posts(&self) -> Vec<&ResponseChildData> {
        let mut posts = Vec::new();
        for resp in &self.responses {
            for child in &resp.data.children {
                posts.push(&child.data);
            }
        }
        posts
    }

    pub async fn get(&mut self, core: &CocoCore) -> anyhow::Result<()> {
        let resp = core.reddit.get(&self.url()).await?;
        let code = resp.status();
        let body = resp.text().await?;
        let redresp = match serde_json::from_str::<RedditResponse>(&body) {
            Ok(redresp) => Ok(redresp),
            Err(err) => {
                error!("Reddit returned a {} response code!", code);
                Err(err)
            }
        }?;
        self.responses.push(redresp);
        Ok(())
    }
}

#[derive(Deserialize)]
pub struct GalleryDataItem {
    pub media_id: String,
    pub id: i64,
}

impl GalleryDataItem {
    pub fn url(&self) -> String {
        format!("https://i.redd.it/{}.jpg", self.media_id)
    }
}

#[derive(Default, Deserialize)]
pub struct GalleryData {
    pub items: Vec<GalleryDataItem>,
}

#[derive(Deserialize)]
pub struct ResponseChildData {
    pub title: String,
    pub id: String,
    pub author: String,
    pub url: String,
    #[serde(default = "bool::default")]
    pub is_gallery: bool,
    #[serde(default = "Option::default")]
    pub gallery_data: Option<GalleryData>,
    pub score: i64,
    pub over_18: bool,
    pub created_utc: f64,
}

impl ResponseChildData {
    pub fn ext(&self) -> String {
        let mut query_cut_str = Vec::new();
        let query_cut = self.url.split('?').collect::<Vec<&str>>();
        for qc in &query_cut {
            query_cut_str.push(qc.to_string());
        }
        let query_start = query_cut_str[0].clone();
        let mut ext_cut_str = Vec::new();
        let ext_cut = query_start.split('.').collect::<Vec<&str>>();
        for ec in &ext_cut {
            ext_cut_str.push(ec.to_string());
        }
        ext_cut_str[ext_cut_str.len() - 1].clone()
    }

    pub fn is_image(&self) -> bool {
        IMG_EXTS.contains(&&*self.ext())
    }
}

#[derive(Deserialize)]
pub struct ResponseChild {
    pub data: ResponseChildData,
}

#[derive(Deserialize)]
pub struct ResponseData {
    pub children: Vec<ResponseChild>,
    pub after: Option<String>,
    pub before: Option<String>,
}

#[derive(Deserialize)]
pub struct RedditResponse {
    pub data: ResponseData,
}
