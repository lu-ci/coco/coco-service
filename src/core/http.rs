use reqwest::{Client, Response};
use std::time::Duration;

pub struct CocoHttpClient;

impl CocoHttpClient {
    pub async fn get(url: &str) -> anyhow::Result<Response> {
        let client = Client::builder()
            .user_agent("coco-service/0.1.3")
            .connect_timeout(Duration::from_secs(30))
            .timeout(Duration::from_secs(60))
            .build()?;
        let resp = client.get(url).send().await?;
        Ok(resp)
    }
}
