use log::info;

use crate::core::service::CocoCore;

mod core;
mod models;
mod services;

#[tokio::main]
async fn main() -> anyhow::Result<()> {
    let mut core = CocoCore::new().await?;
    // core.init().await?;
    if !core.cfg.core.debug {
        loop {
            core.run().await?;
            info!(
                "Waiting {} seconds to start the next cycle...",
                core.cfg.core.interval
            );
            tokio::time::sleep(tokio::time::Duration::from_secs(core.cfg.core.interval)).await;
        }
    } else {
        core.run().await?;
    }
    Ok(())
}
