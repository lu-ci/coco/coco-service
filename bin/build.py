#!/usr/bin/env python

import sys
import subprocess

REGISTRY_BASE = "registry.gitlab.com"
NAME_SPACE = "axaz0r"
PROJECT_NAME = "auto-gallery"

REGISTRY_URI = f"{REGISTRY_BASE}/{NAME_SPACE}/{PROJECT_NAME}"


class Builder(object):
    @staticmethod
    def exec(args):
        subprocess.call(args)

    @staticmethod
    def version():
        ver = None
        ver_line = None
        with open("Cargo.toml", "r") as cargo_file:
            lines = cargo_file.readlines()
            for line in lines:
                if line.startswith("version"):
                    ver_line = line
                    break
        if ver_line:
            ver = ver_line.strip().split("=")[-1].replace("\"", "").strip()
        return ver

    def tags(self):
        tags = []
        version = self.version()
        major, minor, patch = version.split(".")
        versions = [
            "latest",
            f"{major}",
            f"{major}.{minor}",
            f"{major}.{minor}.{patch}"
        ]
        for version in versions:
            tags.append(f"{REGISTRY_URI}:{version}")
        return tags

    def show(self):
        for tag in self.tags():
            print(tag)

    def build(self):
        for tag in self.tags():
            print(f"Building {tag}...")
            self.exec(["docker", "build", ".", "-t", tag])

    def push(self):
        for tag in self.tags():
            print(f"Pushing {tag}...")
            self.exec(["docker", "push", tag])

    def run(self):
        args = sys.argv[1:]
        allowed = ["show", "build", "push"]
        if args:
            jobs = []
            valid = True
            for arg in args:
                arg = arg.lower().strip()
                if arg in allowed:
                    jobs.append(arg)
                else:
                    valid = False
                    break
            if valid:
                for job in jobs:
                    getattr(self, job)()
            else:
                print(f"Invalid job given, only these: {', '.join(allowed)}.")
        else:
            print(f"No jobs given, try one of these: {', '.join(allowed)}.")


if __name__ == "__main__":
    try:
        Builder().run()
    except KeyboardInterrupt:
        print("Keyboard interrupt received, exiting.")
        exit(1)
